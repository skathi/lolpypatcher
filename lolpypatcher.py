#!/usr/bin/env python

"""
BSD 3-Clause License

Copyright (c) 2018, Francisco Martínez
All rights reserved.
"""

import argparse
import codecs
import csv
import distutils.core
import urllib.request
import zlib
from pathlib import Path
from subprocess import run
from time import sleep

launcher_version = ''  # latest release of the game launcher
launcher_urllist = []
launcher_filelist = []
launcher_filesizelist = []
launcher_unpackedfiles = []

game_version = ''  # latest release of the game data
game_urllist = []
game_filelist = []
game_filesizelist = []
game_unpackedfiles = []
game_requiredfiles = ['BsSndRpt.exe',
                      'bugsplat.dll',
                      'bugsplatrc.dll',
                      'cg.dll',
                      'cgD3D9.dll',
                      'cgGL.dll',
                      'D3DCompiler_43.dll',
                      'dbghelp.dll',
                      'League of Legends.exe',
                      'LogitechGkey.dll',
                      'LogitechLed.dll',
                      'stub.dll',
                      'util.dll']

regiondata_version = ''  # latest release of the regional data
regiondata_urllist = []
regiondata_filelist = []
regiondata_filesizelist = []
regiondata_unpackedfiles = []


# Begin helper functions

def request_helper(target):
    return urllib.request.Request(url=target, data=None,
                                  headers={
                                      'User-Agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) '
                                                     'AppleWebKit/537.36 (KHTML, like Gecko) '
                                                     'Chrome/35.0.1916.47 Safari/537.36')
                                  }
                                  )


def get_latest_version_string(target):
    """Returns the first line of the URL at target"""
    req = request_helper(target)
    response = urllib.request.urlopen(req).readline()
    return str(response, 'utf-8').rstrip()


def get_csv_data(target):
    req = request_helper(target)
    response = urllib.request.urlopen(req)
    data = codecs.iterdecode(response, "utf-8")
    csvdata = csv.reader(data)
    next(csvdata)
    return csvdata


# End helper functions

def parse_manifest():
    """Check latest version, download it's package manifest and initialize the core lists"""
    global launcher_version
    launcher_version = get_latest_version_string(
        "http://l3cdn.riotgames.com/releases/live/projects/league_client/releases/releaselisting_NA")
    launcher_csvdata = get_csv_data(
        ("http://l3cdn.riotgames.com/releases/live/"
         "projects/league_client/releases/{}/packages/files/packagemanifest").format(
            launcher_version))

    for i in launcher_csvdata:
        launcher_urllist.append("https://l3cdn.riotgames.com/releases/live" + i[0])
        launcher_filelist.append(i[0][1:])  # strip leading slash to obtain relative paths
        launcher_unpackedfiles.append(i[0][1:-11])  # strip leading slash and remove ".compressed" suffix
        launcher_filesizelist.append(int(i[3]))

    global game_version
    game_version = get_latest_version_string(
        "http://l3cdn.riotgames.com/releases/live/projects/lol_game_client/releases/releaselisting_NA")
    game_csvdata = get_csv_data(
        ("http://l3cdn.riotgames.com/releases/live/"
         "projects/lol_game_client/releases/{}/packages/files/packagemanifest").format(
            game_version))

    for i in game_csvdata:
        if Path(i[0]).stem in game_requiredfiles or '.wad' in Path(
                i[0]).suffixes:  # we don't need everything listed in the packagemanifest :)
            game_urllist.append("https://l3cdn.riotgames.com/releases/live" + i[0])
            game_filelist.append(i[0][1:])
            game_unpackedfiles.append(i[0][1:-11])
            game_filesizelist.append(int(i[3]))

    global regiondata_version
    regiondata_version = get_latest_version_string(
        "http://l3cdn.riotgames.com/releases/live/projects/lol_game_client_en_us/releases/releaselisting_NA")
    regiondata_csvdata = get_csv_data(
        ("http://l3cdn.riotgames.com/releases/live/"
         "projects/lol_game_client_en_us/releases/{}/packages/files/packagemanifest").format(
            regiondata_version))

    for i in regiondata_csvdata:
        regiondata_urllist.append("https://l3cdn.riotgames.com/releases/live" + i[0])
        regiondata_filelist.append(i[0][1:])
        regiondata_unpackedfiles.append(i[0][1:-11])
        regiondata_filesizelist.append(int(i[3]))


def create_folder_structure():
    for i in launcher_filelist:
        Path(i).parent.mkdir(parents=True, exist_ok=True)
    for i in game_filelist:
        Path(i).parent.mkdir(parents=True, exist_ok=True)
    for i in regiondata_filelist:
        Path(i).parent.mkdir(parents=True, exist_ok=True)


def download_content():
    wgetcmd = ['wget',
               '--retry-connrefused',
               '--waitretry=5',
               '--read-timeout=20',
               '--timeout=15',
               '-t 0',
               '--continue',
               '--no-verbose',            # Wget for Windows crashes without this
               '--no-check-certificate',  # The server has a lot of SSL issues for some reason
               '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 '
               '(KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"',
               '-O']

    if args.download_launcher:
        for i, j, k, l in zip(launcher_urllist, launcher_filelist, launcher_filesizelist, launcher_unpackedfiles):
            if not Path(j).exists() or not Path(l).exists() or Path(j).stat().st_size != k:
                while True:
                    if not run(wgetcmd + [j] + [i]).returncode:
                        break
                    sleep(5)

    if args.download_game_data:
        for i, j, k, l in zip(game_urllist, game_filelist, game_filesizelist, game_unpackedfiles):
            if not Path(j).exists() or not Path(l).exists() or Path(j).stat().st_size != k:
                while True:
                    if not run(wgetcmd + [j] + [i]).returncode:
                        break
                    sleep(5)

    if args.download_regional_data:
        for i, j, k, l in zip(regiondata_urllist, regiondata_filelist, regiondata_filesizelist,
                              regiondata_unpackedfiles):
            if not Path(j).exists() or not Path(l).exists() or Path(j).stat().st_size != k:
                while True:
                    if not run(wgetcmd + [j] + [i]).returncode:
                        break
                    sleep(5)


def unzlib_files():
    chunksize = 1048576

    for i, k in zip(launcher_filelist, launcher_unpackedfiles):
        if Path(i).exists():
            print("Unpacking " + i)
            with open(i, 'rb') as src, open(k, 'wb') as dst:
                chunk = src.read(chunksize)
                z = zlib.decompressobj()
                while chunk:
                    dst.write(z.decompress(chunk))
                    chunk = src.read(chunksize)

    for i, k in zip(game_filelist, game_unpackedfiles):
        if Path(i).exists():
            print("Unpacking " + i)
            with open(i, 'rb') as src, open(k, 'wb') as dst:
                chunk = src.read(chunksize)
                z = zlib.decompressobj()
                while chunk:
                    dst.write(z.decompress(chunk))
                    chunk = src.read(chunksize)

    for i, k in zip(regiondata_filelist, regiondata_unpackedfiles):
        if Path(i).exists():
            print("Unpacking " + i)
            with open(i, 'rb') as src, open(k, 'wb') as dst:
                chunk = src.read(chunksize)
                z = zlib.decompressobj()
                while chunk:
                    dst.write(z.decompress(chunk))
                    chunk = src.read(chunksize)


def clean_up():
    """Delete leftover *.compressed files"""
    for i in launcher_filelist:
        if Path(i).exists():
            Path(i).unlink()

    for i in game_filelist:
        if Path(i).exists():
            Path(i).unlink()

    for i in regiondata_filelist:
        if Path(i).exists():
            Path(i).unlink()

# TODO: Add option to force-install each component and error checking to deal with it
# TODO: Also check if previously downloaded files are still valid
# TODO: Perhaps a checksum system of some sort for the above?
# TODO: Review analysis of packagemanifest in case a checksum of


def prepare_for_install():
    """Setup the file hierarchy for the actual install"""
    if args.download_launcher:
        Path('projects/league_client/releases').replace(Path('projects/league_client/managedfiles'))
        Path('projects/league_client/releases/{}/deploy'.format(launcher_version)).mkdir(parents=True, exist_ok=True)
        for i in Path('projects/league_client/managedfiles/').iterdir():
            workdir = i / 'files'
            for j in workdir.iterdir():
                try:
                    j.replace(i / j.name)
                except PermissionError:
                    pass
            distutils.dir_util.remove_tree(str(workdir))
            for file in i.iterdir():
                if file.is_dir():
                    distutils.dir_util.copy_tree(str(file), str(
                        Path('projects/league_client/releases/{}/deploy/{}'.format(launcher_version, file.name))))
                else:
                    distutils.file_util.copy_file(str(file), str(
                        Path('projects/league_client/releases/{}/deploy'.format(launcher_version))))

    if args.download_game_data:
        Path('projects/lol_game_client/releases/{}/deploy'.format(game_version)).mkdir(parents=True, exist_ok=True)
        for i in Path('projects/lol_game_client/releases/').iterdir():
            for j in (i / 'files').iterdir():
                try:
                    j.replace(Path('projects/lol_game_client/releases/{}/deploy'.format(game_version)) / j.name)
                except PermissionError:
                    pass
            if i.name != game_version:
                distutils.dir_util.remove_tree(str(i))
            else:
                for j in i.iterdir():
                    if j.name != 'deploy':
                        if j.is_dir():
                            distutils.dir_util.remove_tree(str(j))
                        else:
                            j.unlink()

    if args.download_regional_data:
        Path('projects/lol_game_client_en_us/releases').replace(Path('projects/lol_game_client_en_us/managedfiles'))
        for i in Path('projects/lol_game_client_en_us/managedfiles/').iterdir():
            workdir = i / 'files'
            for j in workdir.iterdir():
                try:
                    j.replace(i / j.name)
                except PermissionError:
                    pass
            distutils.dir_util.remove_tree(str(workdir))


def main():
    parser = argparse.ArgumentParser(description="League of Legends updater with resumable download support")
    parser.add_argument('-l', action='store_false', dest='download_launcher', default=True, help='Skip processing of '
                                                                                                 'the launcher files')
    parser.add_argument('-g', action='store_false', dest='download_game_data', default=True, help='Skip processing of '
                                                                                                  'the game data files')
    parser.add_argument('-r', action='store_false', dest='download_regional_data', default=True, help='Skip processing'
                                                                                                      'of the regional'
                                                                                                      'data')
    parser.add_argument('-D', action='store_true', dest='no_downloads', default=False, help='Run without downloading '
                                                                                            'any files (same as '
                                                                                            'combining -l -g and -r)')
    parser.add_argument('-z', action='store_false', dest='unpack_files', default=True, help='Run without extracting '
                                                                                            'any files')
    parser.add_argument('-c', action='store_false', dest='cleanup_leftovers', default=True, help='Keep .compressed '
                                                                                                 'files')
    parser.add_argument('-i', action='store_false', dest='organize_hierarchy', default=True, help='Skip organizing '
                                                                                                  'the files into the '
                                                                                                  'official hierarchy')
    global args
    args = parser.parse_args()  # TODO: Make arguments more intuitive

    parse_manifest()
    create_folder_structure()

    if not args.no_downloads:
        download_content()

    if args.unpack_files:
        unzlib_files()

    if args.cleanup_leftovers:
        clean_up()

    if args.organize_hierarchy:
        prepare_for_install()

    print("Done!")


if __name__ == '__main__':
    main()
