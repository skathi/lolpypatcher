# lolPyPatcher

Minimal Python 3 script that fetches the latest version of League of Legends for a manual install. Supports persistent and resumable downloads, unlike the official installer. :)

## Disclaimer

This is free software released under the BSD 3-Clause License, check LICENSE for further information.

lolPyPatcher was created under Riot Games' "Legal Jibber Jabber" policy using assets owned by Riot Games.  Riot Games does not endorse or sponsor this project.

## Why?

Riot Games provides no way to manually download updates for their game, so you're forced to use their official launcher for this purpose. If you happen to be on a slow connection which becomes unstable every now and then, you probably have noticed it would rather discard any progress and start over from 0 than resume the download if the connection is interrupted, which is a little bit irritating (to say the least) when you're downloading a 6GB game over a 1mbps downlink.

This utility seeks to fix this issue.

## How?

First we grab a couple files from the official servers (specifically releaselisting_NA and packagemanifest), to check what the latest version of the game is and bulk download whatever files are need. It then unpacks and sorts the obtained files, replicating the official launcher's file hierarchy to produce an update you can drag and drop to your game directory.

## How do I use it?

Before beginning, note that this script is meant to be used for **fresh installs only**, it will not check what files you already have and it's only been tested with the NA version of the game.

This script requires wget, so if you're on Windows grab a binary from [here](https://eternallybored.org/misc/wget/) and place it in the same directory as the script.

### Steps

- Install the game as normal (you might have to run it once and then close it)
- Run the script
- After it's done, copy over the resulting "projects" folder to the RADS folder in the game's root directory. E.g.: C:\Riot Games\League of Legends\RADS
- Start the official launcher, it should take you to the login screen without having to download anything

## To-do

- Add the ability to parse releasemanifest

## Features, not bugs

- Ironically, if you stop the script at the wrong time it might mess up a file extraction or the sorting function, perhaps causing a complete redownload to be necessary. This is not a bug since the script is only ever meant to be stopped while it's downloading files with wget.